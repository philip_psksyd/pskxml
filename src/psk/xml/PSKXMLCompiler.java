package psk.xml;

import psk.xml.fs.SymbolicFile;
import psk.xml.fs.SymbolicFileSystem;
import psk.xml.type.transformers.TransformerManager;

@SuppressWarnings({"WeakerAccess", "unused"})
public class PSKXMLCompiler {

    protected SymbolicFileSystem inputFileSystem;
    protected SymbolicFileSystem outputFileSystem;
    protected ExceptionHandler exceptionHandler;
    protected TransformerManager transformerManager;

    public PSKXMLCompiler(SymbolicFileSystem inputFileSystem, SymbolicFileSystem outputFileSystem){
        this.inputFileSystem = inputFileSystem;
        this.outputFileSystem = outputFileSystem;
    }

    public void onException(ExceptionHandler exceptionHandler){
        this.exceptionHandler = exceptionHandler;
    }

    public PSKXMLCompiler withTransformerManager(TransformerManager transformerManager){
        this.transformerManager = transformerManager;
        return this;
    }

    public SymbolicFileSystem transform(String path) {
        try {

            SymbolicFile file = this.inputFileSystem.getFile(path);
            if (file != null) {
                XMLDocument document = new XMLDocument(file.getContent(), this.inputFileSystem, this.outputFileSystem, this.transformerManager);
                return document.compile();
            } else {
                throw new Exception("file with path: " + path + " is not present in the file system");
            }

        } catch (Exception ex){
            // send to exception handler
            if(this.exceptionHandler != null){
                this.exceptionHandler.onException(ex);
            } else {
                // ex.printStackTrace();
            }
        }

        return null;
    }

}
