package psk.xml.utils;

public class Path {

    public static String fixPath(String path){

        path = path.replace("//", "/");
        if(path.endsWith("/")){
            path = path.substring(0, path.length() - 2);
        }

        if(!path.startsWith("/")){
            path = "/" + path;
        }

        return path;
    }

}
