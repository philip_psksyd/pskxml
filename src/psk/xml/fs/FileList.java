package psk.xml.fs;

import java.util.ArrayList;

class FileList extends ArrayList<SymbolicFile> {

    void writeAll() throws Exception {
        for (SymbolicFile symbolicFile : this) {
            symbolicFile.write();
        }
    }

}
