package psk.xml.fs;


@SuppressWarnings({"unused", "WeakerAccess"})
public class SymbolicFileSystem {

    protected FileReader fileReader;
    protected FileWriter fileWriter;
    protected FileList fileList = new FileList();

    public SymbolicFileSystem(){

    }

    public void setFileReader(FileReader fileReader){
        this.fileReader = fileReader;
    }

    public void setFileWriter(FileWriter fileWriter){
        this.fileWriter = fileWriter;
    }

    public void addFile(SymbolicFile file){
        file.setFileSystem(this);
        this.fileList.add(file);
    }

    public void writeAll() throws Exception {
        this.fileList.writeAll();
    }

    public void write(SymbolicFile file) throws Exception {
        if(this.fileWriter != null){
            this.fileWriter.write(file);
        } else {
            throw new Exception("No FileWriter specified in " + this.getClass().getName());
        }
    }

    public SymbolicFile getFile(String path) throws Exception {
        if(this.fileReader == null){
            throw new Exception("No FileReader specified in " + this.getClass().getName());
        }

        SymbolicFile file = this.fileReader.read(path);

        if(file == null){
            throw new Exception("No Symbolic file supplied viw FileReader to " + this.getClass().getName());
        }

        return file;
    }

}
