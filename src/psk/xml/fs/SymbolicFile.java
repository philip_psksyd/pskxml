package psk.xml.fs;

import psk.xml.utils.Path;

@SuppressWarnings({"WeakerAccess", "unused"})
public class SymbolicFile {

    protected String content = null;
    protected String fileName = null;
    protected String fullPath = null;
    protected String dirPath = null;
    protected SymbolicFileSystem fileSystem = null;

    public SymbolicFile(String fullPath, String content) {

        this.fullPath = Path.fixPath(fullPath);
        this.content = content;

        this.generateFileInfo();
    }

    protected void generateFileInfo() {
        if (this.fullPath != null) {
            int lastDirSeparatorIndex = this.fullPath.lastIndexOf("/");
            if (lastDirSeparatorIndex >= 0) {
                this.fileName = this.fullPath.substring(lastDirSeparatorIndex + 1);
                this.dirPath = Path.fixPath(this.fullPath.substring(0, lastDirSeparatorIndex));
            } else {
                this.fileName = this.fullPath;
                this.dirPath = "/";
            }
        }
    }

    public SymbolicFileSystem getFileSystem() {
        return fileSystem;
    }

    public void setFileSystem(SymbolicFileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    public void write() throws Exception {
        if(this.fileSystem != null){
            this.fileSystem.write(this);
        } else {
            throw new Exception("no filesystem specified in " + this.getClass().getName() + " for file: " + this.getFullPath());
        }
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFullPath() {
        return fullPath;
    }

    public String getDirPath() {
        return dirPath;
    }

    public void setDirPath(String dirPath) {
        this.dirPath = dirPath;
    }

}
