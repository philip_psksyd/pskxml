package psk.xml.type;

import com.google.gson.annotations.Expose;
import org.w3c.dom.*;
import psk.xml.XMLDocument;
import psk.xml.type.transformers.TransformerManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"WeakerAccess", "unused"})
public class XMLType implements java.io.Serializable {

    @Expose
    String type;
    @Expose
    Map<String, Object> attributes = new HashMap<>();
    @Expose
    ArrayList<XMLType> children = new ArrayList<>();
    @Expose
    String text;

    XMLType parent = null;

    public XMLType(){
    }

    public XMLType(XMLType parent){
        this.parent = parent;
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    public XMLType clone(){
        XMLType newType = new XMLType(this.getParent());

        newType.setText(this.getText());
        newType.setAttributes(this.getAttributes());
        newType.setType(this.getType());
        newType.setChildren(this.getChildren());

        return newType;
    }

    public void setParent(XMLType parent){
        this.parent = parent;
    }

    public XMLType getParent(){
        return this.parent;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public Object getAttribute(String attribute){
        return this.getAttributes().get(attribute);
    }

    public boolean hasAttribute(String attribute){
        return this.getAttributes().containsKey(attribute);
    }

    public void setAttribute(String attribute, String value){
        this.getAttributes().put(attribute, value);
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public void addAttributes(Map<String, Object> attributes){
        this.attributes.putAll(attributes);
    }

    public ArrayList<XMLType> getChildren() {
        return children;
    }

    public XMLType getChild(String find){
        for(int i = 0; i < this.getChildren().size(); i++){
            XMLType child = this.getChildren().get(i);

            if(child.getType().equalsIgnoreCase(find)){
                return child;
            }
        }

        return null;
    }

    public void setChildren(ArrayList<XMLType> children) {
        for(XMLType child : this.children){
            // remove current children's parent
            child.setParent(this);
        }

        for(XMLType child : children){
            // add new children's parent
            child.setParent(this);
        }
        this.children = children;
    }

    public void mergeChildren(XMLType with){
        this.mergeChildren(with.getChildren());
    }

    public void mergeChildren(ArrayList<XMLType> toBeMerged){
        this.children.addAll(toBeMerged);
        this.setChildrenParent();
    }

    public void replaceChild(XMLType child, XMLType newChild){
        ArrayList<XMLType> list = new ArrayList<>();
        list.add(newChild);
        this.replaceChild(child, list);
    }

    public void replaceChild(XMLType child, ArrayList<XMLType> newChildren){
        int index = this.children.indexOf(child);
//        for(int i = newChildren.size() - 1; i >= 0; i--){
//            this.children.add(index, newChildren.get(i));
//        }

//        for(int i = 0; i < newChildren.size(); i++){
//            this.children.add(index, newChildren.get(i));
//        }

        this.children.addAll(index, newChildren);

        this.children.remove(child);
    }

    private void setChildrenParent(){
        for(XMLType child : this.children){
            child.setParent(this);
        }
    }

    public void appendChild(XMLType newChild){
        newChild.setParent(this);
        this.children.add(newChild);
    }

    public void removeChild(XMLType child){
        child.setParent(null);
        this.children.remove(child);
    }

    public ArrayList<XMLType> findTagRecursive(String tagName){
        ArrayList<XMLType> list = new ArrayList<XMLType>();

        for(XMLType child : this.getChildren()){
            if(child.getType().equalsIgnoreCase(tagName)){
                list.add(child);
            }

            ArrayList<XMLType> childFound = child.findTagRecursive(tagName);
            if(childFound != null && childFound.size() > 0){
                list.addAll(childFound);
            }
        }

        return list;
    }

    public static Element toElement(XMLType type, Document document){
        Element element = document.createElement(type.getType());
        for (Map.Entry<String, Object> attribute : type.getAttributes().entrySet()) {
            element.setAttribute(attribute.getKey(), attribute.getValue().toString());
        }

        if(!type.getChildren().isEmpty()){
            for(XMLType child : type.getChildren()){
                Element childElement = toElement(child, document);

                element.appendChild(childElement);
            }
        } else if(type.getText() != null && !type.getText().isEmpty()){
            CDATASection textNode = document.createCDATASection(type.getText());
            element.appendChild(textNode);
        }

        return element;
    }

    public static XMLType resolve(Element element, XMLType parent, TransformerManager transformerManager) throws Exception {
        XMLType type = new XMLType(null);

        Map<String, Object> attributes = new HashMap<>();
        ArrayList<XMLType> children = new ArrayList<>();
        NamedNodeMap attributeMap = element.getAttributes();
        type.setType(element.getTagName().toLowerCase());

        for (int i = 0; i < attributeMap.getLength(); i++) {
            Node node = attributeMap.item(i);
            String attributeName = node.getNodeName();
            if (attributeName.startsWith("xmlns:")) {
                continue;
            }
            String attributeValue = node.getNodeValue();

            attributes.put(attributeName, attributeValue);
        }

        for (int i = 0; i < element.getChildNodes().getLength(); i++) {
            Node node = element.getChildNodes().item(i);

            if(node instanceof CharacterData){
                CharacterData characterData = (CharacterData) node;
                String data = characterData.getData().trim();

                if(JavascriptHandler.isJavascript(element)){
                    data = JavascriptHandler.compress(data);
                }

                if(!data.isEmpty()){
                    type.setText(data);
                }

            } else {
                switch (node.getNodeType()) {
                    case Node.ELEMENT_NODE:
                        Element child = (Element) node;
                        children.add(XMLType.resolve(child, type, transformerManager));
                        break;
                    case Node.TEXT_NODE:
                        type.setText(node.getTextContent().trim());
                        break;
                }
            }
        }


        type.setAttributes(attributes);
        type.setChildren(children);

        if(transformerManager != null) {
            type = transformerManager.transform(type);
        }

        return type;
    }

}
