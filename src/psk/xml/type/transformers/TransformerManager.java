package psk.xml.type.transformers;


import psk.xml.type.XMLType;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"WeakerAccess", "unused"})
public class TransformerManager {

    protected Map<String, Class<? extends Transformer>> transformerMap = new HashMap<>();

    public void addTransformer(String typeName, Class<? extends Transformer> transformerClass){
        this.transformerMap.put(typeName, transformerClass);
    }

    public XMLType transform(XMLType type) throws Exception {

        String typeName = type.getType().toLowerCase();

        if (this.transformerMap.containsKey(typeName)) {
            Class<? extends Transformer> transformerClass = this.transformerMap.get(typeName);
            Constructor<?> constructor = transformerClass.getConstructor(XMLType.class);
            Object transformerObject = constructor.newInstance(type);
            ((Transformer) transformerObject).transform();
        }

        return type;
    }

}
