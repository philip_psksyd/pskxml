package psk.xml.type.transformers;


import psk.xml.type.XMLType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Form extends Transformer {

    private Map<String, XMLType> fieldMap = new HashMap<>();

    public Form(XMLType type) {
        super(type);
    }

    private XMLType parseFields(XMLType type){
        ArrayList<XMLType> children = type.getChildren();
        ArrayList<XMLType> newChildren = new ArrayList<>();
        for (XMLType child : children) {
            if (child.getType().equalsIgnoreCase("field")) {
                String fieldName = (String) child.getAttribute("name");
                if (fieldName != null && this.fieldMap.containsKey(fieldName)) {
                    XMLType field = this.fieldMap.get(fieldName);
                    field.addAttributes(child.getAttributes());
                    child = field;
                    child.setType("field");
                }
            }

            child = this.parseFields(child);
            newChildren.add(child);
        }

        type.setChildren(newChildren);
        return type;
    }

    @Override
    public XMLType transform() {

        XMLType fieldDeclarations = this.getChild("fields");
        for(XMLType child : fieldDeclarations.getChildren()){
            String childName = (String) child.getAttribute("name");
            if(childName != null) {
                this.fieldMap.put(childName, child.clone());
            }
        }

        ArrayList<XMLType> newChildren = new ArrayList<>();
        for(XMLType child : type.getChildren()){

            if(child.getType().equalsIgnoreCase("layout")){
                child = this.parseFields(child);
            }

            newChildren.add(child);
        }
        type.setChildren(newChildren);

        return type;
    }
}
