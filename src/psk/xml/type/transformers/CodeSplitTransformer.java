package psk.xml.type.transformers;

import psk.xml.type.XMLType;
import psk.xml.utils.copy.DeepCopy;

import java.util.*;

/**
 * @author Philip Andersson
 *
 * this class is looking for any async-types that will be splited into new files for async loading
 */

public class CodeSplitTransformer {

    protected XMLType type;

    public CodeSplitTransformer(XMLType type){

        this.type = type;

    }

    public Map<String, XMLType> transform() throws Exception {
        Map<String, XMLType> parts = new HashMap<>();

        this.type = this.parseType(this.type, parts);

        parts.put("index", this.type);

        return parts;
    }

    protected XMLType parseType(XMLType type, Map<String, XMLType> parts) throws Exception {

        Iterator<XMLType> ite = type.getChildren().iterator();
        ArrayList<XMLType> newChildren = new ArrayList<>();

        while(ite.hasNext()){
            XMLType child = ite.next();
            newChildren.add(this.parseType(child, parts));
        }

        type.setChildren(newChildren);

        if(type.hasAttribute("async") && type.getAttribute("async").toString().equalsIgnoreCase("true")){

            String hash = UUID.randomUUID().toString();
            XMLType asyncComponent = new XMLType(null);
            asyncComponent.setType("async-component");
            asyncComponent.setAttribute("hash", hash);

            XMLType part = (XMLType) DeepCopy.copy(type);

            parts.put(hash, part);

            return asyncComponent;

        }

        return type;

    }

}
