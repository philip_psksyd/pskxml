package psk.xml.type.transformers;

import psk.xml.type.XMLType;

public abstract class Transformer implements java.io.Serializable {

    protected XMLType type;

    Transformer(XMLType type) {
        this.type = type;
    }

    XMLType getChild(String type){
        return this.getChild(type, null);
    }

    XMLType getChild(String find, XMLType in){
        if(in == null){
            in = type;
        }

        for(int i = 0; i < in.getChildren().size(); i++){
            XMLType child = in.getChildren().get(i);

            if(child.getType().equalsIgnoreCase(find)){
                return child;
            }
        }

        return null;
    }

    public abstract XMLType transform() throws Exception;

}
