package psk.xml.type.transformers;

import psk.xml.utils.Pair;
import psk.xml.utils.copy.DeepCopy;
import psk.xml.type.XMLType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class SystemType extends Transformer implements java.io.Serializable {

    private ArrayList<XMLType> views = new ArrayList<>();
    private ArrayList<XMLType> workflows = new ArrayList<>();

    public SystemType(){
        super(null);
    }

    public SystemType(XMLType type) {
        super(type);
    }

    private void collectViews(String moduleName, XMLType views){
        if(views != null) {
            for (XMLType view : views.getChildren()) {
                // add views to global view type
                view.setAttribute("name", moduleName + "/" + view.getAttribute("name"));

                this.views.add(view);
            }
        }
    }

    private void collectWorkflows(String moduleName, XMLType workflows){
        if(workflows != null) {
            for (XMLType workflow : workflows.getChildren()) {
                // add workflows to global workflow type
                workflow.setAttribute("name", moduleName + ":" + workflow.getAttribute("name"));

                this.workflows.add(workflow);
            }
        }
    }

    private void collectForms(String moduleName, XMLType forms){
        if(forms != null) {
            for (XMLType form : forms.getChildren()) {
                // add views to global view type
                String name = moduleName + ":" + form.getAttribute("name");
                form.setAttribute("name", name);
                if(form.hasAttribute("parent")){
                    String parent = (String) form.getAttribute("parent");
                    if(!parent.contains(":")) {
                        form.setAttribute("parent", moduleName + ":" + parent);
                    }
                } else {
                    form.setAttribute("parent", "");
                }

                FormManager.addForm(name, new SystemForm(form));
            }
        }
    }

    private void createCollection(String name, ArrayList<XMLType> children){
        XMLType collection = new XMLType();
        collection.setType(name);
        collection.setChildren(children);
        type.appendChild(collection);
    }

    private ArrayList<XMLType> peelChildContainers(ArrayList<XMLType> children){

        ArrayList<Pair<XMLType, ArrayList<XMLType>>> replace = new ArrayList<>();

        for(XMLType child : children){

            child.setChildren(this.peelChildContainers(child.getChildren()));

            if(child.getType().equalsIgnoreCase("child-container")){
                ArrayList<XMLType> newChildren = new ArrayList<>();

                for(XMLType ccChild : child.getChildren()){
                    newChildren.add((XMLType) DeepCopy.copy(ccChild));
                }

                replace.add(new Pair<>(child, newChildren));
            }
        }

        for(Pair<XMLType, ArrayList<XMLType>> pair : replace){
            pair.getFirst().getParent().replaceChild(pair.getFirst(), pair.getSecond());
        }

        return children;
    }

    private XMLType peelChildContainers(XMLType type){
        for(XMLType child : type.getChildren()){
            child = this.peelChildContainers(child);
            if(child.getType().equalsIgnoreCase("child-container")){
                ArrayList<XMLType> newChildren = new ArrayList<>();

                for(XMLType ccChild : child.getChildren()){
                    newChildren.add((XMLType) DeepCopy.copy(ccChild));
                }

                type.replaceChild(child, newChildren);
            }
        }

        return type;
    }

    @Override
    public XMLType transform() throws Exception {

        XMLType modules = this.getChild("modules");
        for(XMLType module : modules.getChildren()){
            String moduleName = (String) module.getAttribute("name");

            XMLType views = this.getChild("views", module);
            this.collectViews(moduleName, views);

            XMLType forms = this.getChild("forms", module);
            this.collectForms(moduleName, forms);

            ArrayList<XMLType> createFormTags = module.findTagRecursive("form-create");
            for(XMLType formCreate : createFormTags){
                formCreate.setAttribute("form", moduleName + ":" + ((String)formCreate.getAttribute("form")));
            }
        }

        ArrayList<XMLType> forms = null;
        forms = FormManager.buildAll();
        forms = this.peelChildContainers(forms);

        this.createCollection("forms", forms);
        this.createCollection("views", this.views);

        type.removeChild(this.getChild("modules"));

        return type;
    }

    private static class FormManager {
        private static Map<String, SystemForm> managerForms = new HashMap<>();

        static void addForm(String name, SystemForm form){
            managerForms.put(name, form);
        }

        static SystemForm getForm(String name){
            return managerForms.get(name);
        }

        static boolean hasForm(String name){
            return managerForms.containsKey(name);
        }

        static ArrayList<XMLType> buildAll() throws Exception {
            ArrayList<XMLType> list = new ArrayList<>();
            for(Map.Entry<String, SystemForm> systemForm : managerForms.entrySet()){
                list.add(systemForm.getValue().build());
            }

            return list;
        }
    }

    private class SystemForm implements java.io.Serializable {

        private XMLType form;
        private SystemForm parent;
        private String parentName;
        private boolean hasParent = false;
        private Map<String, XMLType> childContainers;
        private boolean built = false;
        private XMLType layout;
        private XMLType fields;
        private Workflow workflow;
        private boolean hasWorkflow = false;
        String name;

        SystemForm(XMLType form){
            this.form = form;
            this.name = this.form.getAttribute("name").toString();
//            this.childContainers = this.getChildContainers(this.form);

            this.parentName = (String) form.getAttribute("parent");
            this.hasParent = !this.parentName.isEmpty();

            XMLType workflowTag = this.getWorkflow();
            if(workflowTag != null && workflowTag.hasAttribute("built")){
                this.workflow = new Workflow(workflowTag);
            }
        }

        private XMLType getLayout(){
            return this.form.getChild("layout");
        }

        private XMLType getWorkflow(){
            return this.form.getChild("workflow");
        }

        private XMLType getFields(){
            return this.form.getChild("fields");
        }

        void buildWorkflow(){
            XMLType workflow = this.getWorkflow();
            boolean append = false;
            if(workflow == null){
                workflow = new XMLType();
                workflow.setType("workflow");
                append = true;
            }

            this.workflow = new Workflow(workflow);

            this.hasWorkflow = true;

            System.out.println("workflow set in: " + this.name);

            if(this.parent != null) {
                this.parent.buildWorkflow();
                System.out.println("merge workflow: " + this.name + " with parent: " + this.parent.name);
                this.workflow.merge(this.parent.workflow);
            }

            XMLType newWorkflow = this.workflow.build();
            newWorkflow.setAttribute("built", "true");

            if(!append) {
//                this.form.replaceChild(workflow, newWorkflow);
                this.form.removeChild(workflow);

            }
            this.form.appendChild(newWorkflow);

//            this.form.replaceChild(workflow, newWorkflow);
        }

        XMLType build() throws Exception {

            if(this.form.hasAttribute("built")){
                return this.form;
            }

            // check and resolve parent
            if(this.hasParent && FormManager.hasForm(this.parentName)){
                this.parent = (SystemForm) DeepCopy.copy(new SystemForm(FormManager.getForm(this.parentName).build()));
            } else {
                this.form.setAttribute("built", "true");
                return this.form;
            }

            XMLType layout = this.getLayout();

            if(layout != null){
                for(XMLType fragment : layout.getChildren()){
                    if(fragment.hasAttribute("render-in")) {
                        String renderIn = ((String) fragment.getAttribute("render-in")).toLowerCase();
                        if(this.parent.getChildContainers().containsKey(renderIn)){
                            XMLType childContainer = this.parent.getChildContainers().get(renderIn);
                            childContainer.appendChild(fragment);
                        } else {
                            // error handling
                            throw new Exception("cant find child container with id: " + renderIn + " for form: " + this.form.getAttribute("name"));
                        }
                    }
                }
            }

            XMLType parentFields = this.parent.getFields();
            if(parentFields != null) {
                this.getFields().mergeChildren(parentFields);
            }
            this.getLayout().setChildren(this.parent.getLayout().getChildren());
            this.form.setAttribute("built", "true");

            this.buildWorkflow();

            return this.form;
        }

        XMLType getForm(){
            return this.form;
        }

        private Map<String, XMLType> getChildContainers(){
            return this.getChildContainers(null);
        }

        private Map<String, XMLType> getChildContainers(XMLType type){
            if(type == null){
                type = this.form.getChild("layout");
            }

            Map<String, XMLType> map = new HashMap<>();

            for(XMLType child : type.getChildren()){
                if(child.getType().equalsIgnoreCase("child-container")){
                    // got child-container
                    map.put(((String) child.getAttribute("id")).toLowerCase(), child);
                }

                map.putAll(this.getChildContainers(child));
            }

            return map;
        }

    }

    private class Workflow implements java.io.Serializable {
        private XMLType workflow;
        private TransitionList transitions;

        Workflow(XMLType workflow){
            this.workflow = workflow;
            this.transitions = new TransitionList().fromXML(this.workflow.getChild("transitions"));
        }

        void merge(Workflow parentWorkflow){
            System.out.println(parentWorkflow);
            this.transitions.merge(parentWorkflow.transitions);
        }

        XMLType build(){
            XMLType workflow = new XMLType();
            workflow.setType("workflow");

            for(Transition transition : this.transitions){
                workflow.appendChild(transition.transition);
            }

            return workflow;
        }

    }

    private class TransitionList extends ArrayList<Transition> implements java.io.Serializable {

        TransitionList fromXML(XMLType transitions){
            for(XMLType transition : transitions.getChildren()){
                this.add(new Transition(transition));
            }

            return this;
        }

        void merge(TransitionList transitions){

            for(int i = 0; i < this.size(); i++){
                Transition transition = this.get(i);

                if(transition.isSuper){

                    this.remove(i);
                    this.addAll(i, transitions);

                    return;

                }

            }

        }

    }

//    private class StepList extends ArrayList<Step> {}

    private class Transition implements java.io.Serializable {
        XMLType transition;
        String from;
        String to;
        boolean isSuper;
//        StepList steps = new StepList();

        Transition(XMLType transition){
            this.transition = transition;

            if(this.transition.getType().equalsIgnoreCase("super")){
                this.isSuper = true;
            } else {
                this.from = transition.getAttribute("from").toString();
                this.to = transition.getAttribute("to").toString();
            }


//            this.createStepList();
        }

//        void createStepList(){
//            for(XMLType step : this.transition.getChildren()){
//                this.steps.add(new Step(step));
//            }
//        }

    }
//
//    private class Step {
//
//        XMLType step;
//        boolean isSuper;
//
//        Step(XMLType step){
//            this.step = step;
//            this.isSuper = this.step.getType().equalsIgnoreCase("super");
//        }
//
//    }

}
