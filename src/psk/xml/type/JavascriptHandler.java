package psk.xml.type;


import com.yahoo.platform.yui.compressor.JavaScriptCompressor;
import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.*;

@SuppressWarnings({"unused", "WeakerAccess"})
public class JavascriptHandler {

    private static List<String> javascriptTypes = Arrays.asList("event");
    private static List<String> warnings = new ArrayList<>();
    private static List<String> errors = new ArrayList<>();

    public static boolean isJavascript(Element element) {
        Node child = element.getFirstChild();
        return child instanceof CharacterData && javascriptTypes.contains(element.getTagName());
    }

    public static String compress(String data) throws Exception {
        if(data.trim().isEmpty()){
            return "";
        }
        StringReader stringReader = new StringReader(data);
        Writer writer = new StringWriter();
        warnings = new ArrayList<>();
        errors = new ArrayList<>();

        JavaScriptCompressor compressor = new JavaScriptCompressor(stringReader, new ErrorReporter() {
            @Override
            public void warning(String s, String s1, int i, String s2, int i1) {
                warnings.add(s);
            }

            @Override
            public void error(String s, String s1, int i, String s2, int i1) {
                errors.add(s);
            }

            @Override
            public EvaluatorException runtimeError(String s, String s1, int i, String s2, int i1) {
                errors.add(s);
                return null;
            }
        });

        if(!errors.isEmpty()){
            throw new Exception("errors occured in javascript: " + errors.toString());
        }

        stringReader.close(); stringReader = null;
        compressor.compress(writer, -1, true, false, true, false);

        return writer.toString();
    }

}
