package psk.xml;

import psk.xml.fs.*;
import psk.xml.fs.FileReader;
import psk.xml.fs.FileWriter;
import psk.xml.type.transformers.Form;
import psk.xml.type.transformers.SystemType;
import psk.xml.type.transformers.TransformerManager;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

class Test {

    public static void main(String[] args) {


        String writePath = "C:\\Users\\PA\\IdeaProjects\\PSKXML\\examplefiles\\parts\\";
        String readPath = "C:\\Users\\PA\\IdeaProjects\\PSKXML\\examplefiles\\";

        // clean output directory
        File dir = new File(writePath);
        for (File file : dir.listFiles())
            if (!file.isDirectory())
                file.delete();

        SymbolicFileSystem fileSystem = new SymbolicFileSystem();
        SymbolicFileSystem output = new SymbolicFileSystem();


        TransformerManager man = new TransformerManager();
        man.addTransformer("form", Form.class);
        man.addTransformer("system", SystemType.class);

        fileSystem.setFileReader(new FileReader() {
            @Override
            public SymbolicFile read(String path) throws Exception {
                try {
                    return new SymbolicFile(path, readFile(readPath + path, Charset.defaultCharset()));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }
        });

        output.setFileWriter(new FileWriter() {
            @Override
            public void write(SymbolicFile file) throws Exception {
                System.out.println("writing: " + file.getFullPath());
                String path = writePath + file.getFullPath();
                writeFile(path, file.getContent());
            }
        });


        PSKXMLCompiler compiler = new PSKXMLCompiler(fileSystem, output);
        compiler.onException(new ExceptionHandler() {
            @Override
            public void onException(Exception ex) {
                System.out.println("got exception, darn it!");
                ex.printStackTrace();
            }
        });
        try {
            SymbolicFileSystem out = compiler.withTransformerManager(man).transform("system.xml");
            System.out.println("\n\ncompiler done -------------------------------------------");
            out.writeAll();
        } catch (Exception e) {
            // e.printStackTrace();
        }


    }

    private static void writeFile(String path, String content) throws IOException {
        File configFile = new File(path);
        java.io.FileWriter fw = new java.io.FileWriter(configFile);
        fw.write(content);
        fw.close();
    }

    private static String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

}
