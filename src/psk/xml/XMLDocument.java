package psk.xml;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import psk.xml.fs.SymbolicFile;
import psk.xml.fs.SymbolicFileSystem;
import psk.xml.type.XMLType;
import psk.xml.type.transformers.CodeSplitTransformer;
import psk.xml.type.transformers.TransformerManager;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"WeakerAccess", "unused"})
public class XMLDocument {

    protected String xmlString;
    protected Document document;
    protected Transformer transformer;
    protected Element rootElement;
    protected XMLType rootType;
    protected SymbolicFileSystem inputFileSystem;
    protected SymbolicFileSystem outputFileSystem;
    protected Map<String, XMLType> parts = null;
    protected TransformerManager transformerManager;

    public XMLDocument(String xmlString, SymbolicFileSystem inputFileSystem, SymbolicFileSystem outputFileSystem, TransformerManager transformerManager) {
        this.xmlString = xmlString;
        this.inputFileSystem = inputFileSystem;
        this.outputFileSystem = outputFileSystem;
        this.transformerManager = transformerManager;
    }

    protected void createDocument() throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        this.document = builder.parse(
                new ByteArrayInputStream(
                        this.xmlString.getBytes(StandardCharsets.UTF_8)));

        this.rootElement = this.document.getDocumentElement();
    }

    protected void resolve() throws Exception {
        this.rootType = XMLType.resolve(this.rootElement, null, this.transformerManager);
    }

    protected void codeSplit() throws Exception {
        if (this.parts != null) {
            return;
        }

        CodeSplitTransformer transformer = new CodeSplitTransformer(this.rootType);
        this.parts = transformer.transform();
    }

    protected void replaceIncludes(XMLDocument document) throws Exception {
        Element root = document.rootElement;

        this.replaceIncludes(root);
    }

    protected void replaceIncludes(Element root) throws Exception {

        if (root == null) {
            throw new Exception("root is null");
        }

        NodeList childNodes = root.getChildNodes();

        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                if (element.getTagName().equalsIgnoreCase("include")) {
                    // replace me

                    String path = element.getAttribute("file");
                    SymbolicFile file = this.inputFileSystem.getFile(path);

                    if (file != null) {
                        XMLDocument fileDocument = new XMLDocument(file.getContent(), this.inputFileSystem, this.outputFileSystem, this.transformerManager);

                        fileDocument.transform();

                        Node newNode = this.document.importNode(fileDocument.rootElement, true);
                        element.getParentNode().replaceChild(newNode, element);
                    }
                }

                this.replaceIncludes(element);
            }
        }
    }

    protected Map<String, String> toJsonParts() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Map<String, String> parts = new HashMap<>();
        for (Map.Entry<String, XMLType> part : this.parts.entrySet()) {
            parts.put(part.getKey(), gson.toJson(part.getValue()));
        }

        return parts;
    }

    protected Map<String, String> toXMLParts() throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Map<String, String> parts = new HashMap<>();
        for (Map.Entry<String, XMLType> part : this.parts.entrySet()) {
            Document document = builder.newDocument();
            Element rootElement = XMLType.toElement(part.getValue(), document);
            document.appendChild(rootElement);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);

            this.transform(result, document);

            parts.put(part.getKey(), writer.toString());
        }

        return parts;
    }

    public String toJson() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return gson.toJson(this.rootType);
    }

    public String toXML() {
        try {

            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.newDocument();
            Element rootElement = XMLType.toElement(this.rootType, document);
            document.appendChild(rootElement);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);

            this.transform(result, document);

            return writer.toString();

        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }

        return null;
    }

    protected void create() throws Exception {
        this.createDocument();
        this.replaceIncludes(this);
    }

    protected void transform() throws Exception {
        this.create();
        this.resolve();
        this.codeSplit();
    }

    protected SymbolicFileSystem compile() throws Exception{
        this.transform();

        // xml files
        for(Map.Entry<String, String> part : this.toXMLParts().entrySet()){
            this.outputFileSystem.addFile(new SymbolicFile(part.getKey() + ".xml", part.getValue()));
        }

        // json files
        for(Map.Entry<String, String> part : this.toJsonParts().entrySet()){
            this.outputFileSystem.addFile(new SymbolicFile(part.getKey() + ".json", part.getValue()));
        }

        return this.outputFileSystem;
    }

    protected void transform(Result result) throws TransformerException {
        this.transform(result, this.document);
    }

    protected void transform(Result result, Document document) throws TransformerException {
        this.getTransformer().transform(new DOMSource(document), result);
    }

    protected Transformer getTransformer() throws TransformerConfigurationException {
        if (this.transformer == null) {
            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            this.transformer = tr;
        }

        return this.transformer;
    }

}
