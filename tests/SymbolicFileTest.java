import org.junit.Assert;
import org.junit.Test;
import psk.xml.fs.SymbolicFile;

public class SymbolicFileTest {

    @Test
    public void fileInfo(){
        SymbolicFile rootFile = new SymbolicFile("inRoot.xml", "");
        Assert.assertEquals("filename is correct when in root path", "inRoot.xml", rootFile.getFileName());

        SymbolicFile longFile = new SymbolicFile("/path/to/file.xml", "");
        Assert.assertEquals("filename is correct when in longer path", "file.xml", longFile.getFileName());
    }

    @Test
    public void dirInfo(){
        SymbolicFile rootFile = new SymbolicFile("inRoot.xml", "");
        Assert.assertEquals("filename is correct when in root path", "/", rootFile.getDirPath());

        SymbolicFile longFile = new SymbolicFile("/path/to/file.xml", "");
        Assert.assertEquals("filename is correct when in longer path", "/path/to", longFile.getDirPath());
    }

    @Test
    public void gettersAndSetters(){
        SymbolicFile file = new SymbolicFile("test.xml", "");

        file.setFileName("test2.xml");
        Assert.assertEquals("get/setFileName", "test2.xml", file.getFileName());

        file.setContent("content");
        Assert.assertEquals("get/setContent", "content", file.getContent());

    }

}
